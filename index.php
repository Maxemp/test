<?php
	require_once('connect.php');
	$query = "SELECT * FROM workers";
	$wor = $pdo->query($query);
	if (!empty($_GET['del_id'])) {
		$del_id = $_GET['del_id'];
		$del_query = "DELETE FROM workers WHERE id = $del_id";
		$pdo->exec($del_query);
		header('Location: index.php');
	}
	if (!(empty($_GET['nam']) && empty($_GET['age']) && empty($_GET['salary']))) {
		$n = $_GET['nam'];
		$a = $_GET['age'];
		$s = $_GET['salary'];
		$vals = [
			':name' => $n,
			':age' => $a,
			':salary' => $s
		];
		$add_query = "INSERT INTO `workers`(`id`, `name`, `age`, `salary`) VALUES (NULL, :name, :age, :salary)";
		$w = $pdo->prepare($add_query);
		$w->execute($vals);
		header('Location: index.php');
	}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
 <meta charset='utf-8'>
</head>
<body>
		<table border="1">
			<caption>Таблица зарплат</caption>
			<tr>
				<th>id</th>
				<th>name</th>
				<th>age</th>
				<th>salary</th>
				<th>delete</th>
				<th>редактировать</th>
			</tr>
		<?php while($worker = $wor->fetch()):?>
			<form action="index.php">
				<tr>
					<td><?= $worker['id']?></td>
					<td><?= $worker['name']?></td>
					<td><?= $worker['age']?></td>
					<td><?= $worker['salary']?></td>
					<td><a href="index.php?del_id=<?= $worker['id']?>">удалить</a></td>
					<td><a href="update.php?up_id=<?= $worker['id']?>">редактировать</a></td>
				</tr>
			</form>
		<?php endwhile;?>
		</table>
		<form action="index.php">
			Имя работяги: <input type="text" name="nam"><br/>
			Возраст работяги: <input type="text" name="age"><br/>
			Зарплата работяги: <input type="text" name="salary"><br/>
			<input type="submit" name="submit">
		</form>
</body>
</html>