<?php
	require_once("connect.php");

	if (!empty($_GET['up_id'])) {
		$query = "SELECT * FROM workers WHERE id = :up_id";
		try{
			$w = $pdo->prepare($query);
			$up_id = $_GET['up_id'];
			$w->execute([':up_id' => $up_id]);
			$worker = $w->fetch();
		}catch(PDOException $e){
			echo "Невозможно найти работягу: ".$e->getMessage();
		}
	}
	if (!(empty($_GET['name'] && $_GET['age'] && $_GET['salary']))) {
		$values = [
			':name' => $_GET['name'],
			':age' => $_GET['age'],
			':salary' => $_GET['salary'],
			':id' => $_GET['id']
		];
		try{
			$query = "UPDATE workers SET name=:name age=:age salary=:salary WHERE id=:id";
			$u = $pdo->prepare($query);
			$u->execute($values);
			//header("Location: index.php");
		}catch(PDOException $e){
			echo "Невозможно изменить работягу: ".$e->getMessage();
		}
		
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php if (!empty($_GET['up_id'])): ?>
		<form action="index.php">
			Name: <input type="text" name="name" value="<?= $worker['name']?>"><br/>
			Age: <input type="text" name="age" value="<?= $worker['age']?>"><br/>
			salary: <input type="text" name="salary" value="<?= $worker['salary']?>"><br/>
			<input type="hidden" name="id" value="<?= $worker['id']?>">
			<input type="submit" name="123">
		</form>
	<?php endif; ?>
</body>
</html>